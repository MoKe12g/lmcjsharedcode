package de.creeperit.lmcprotocol.PRCCommands;

public class prcRegistrar implements prcCommand {
    @Override
    public String getShort() {
        return "reg";
    }

    @Override
    public String getLong() {
        return "registrar";
    }
}
