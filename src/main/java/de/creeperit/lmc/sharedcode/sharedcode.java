package de.creeperit.lmc.sharedcode;

import de.creeperit.lmcprotocol.PRCCommands.prcRegistrar;
import de.creeperit.lmcprotocol.Protocol;

public class sharedcode {

    public static void setupProtocol() {

        // let the protocol init
        Protocol.init();

        // modifier protocol commands

        Protocol.prcCommandList.add(new prcRegistrar());

    }

}
